# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect

# Create your views here.

from .models import form 
from .forms import formForm

def update(request,update_id):
	akun_update = form.objects.get(id=update_id)
	
	data = {
		'nama'     			 : akun_update.nama,
		'email'    		     : akun_update.email,
		'jenis_kelamin'      : akun_update.jenis_kelamin,
		'alamat'       		 : akun_update.alamat,
	}

	akun_form = formForm(request.POST or None, initial=data, instance=akun_update)

	if request.method == 'POST':
		if akun_form.is_valid():
			akun_form.save()
		return redirect('registrasi:list')

	context = {
		'judul':'Jurnal Registrasi',
		'subjudul':'Ubah Registrasi',
		"page_title":"Ubah Akun",
		"akun_form":akun_form,
	}
	return render(request,'registrasi/create.html',context)


def delete(request,delete_id):
	form.objects.filter(id=delete_id).delete()
	return redirect('registrasi:list')

def create(request):
	akun_form = formForm(request.POST or None)

	if request.method == 'POST':
		if akun_form.is_valid():
			akun_form.save()
		return redirect('registrasi:list')

	context = {
		'judul':'Jurnal Registrasi',
		'subjudul':'Tambah Registrasi',
		"page_title":"Tambah Akun",
		"akun_form":akun_form,
	}
	return render(request,'registrasi/create.html',context)

def list(request):
	semua_akun = form.objects.all()

	context = {
		'judul':'Jurnal Registrasi',
		'subjudul':'Form Registrasi',
		'page_title':'Registrasi',
		'semua_akun':semua_akun,
	}
	return render(request,'registrasi/list.html',context)